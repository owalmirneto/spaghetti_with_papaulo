<header class="container">
  <!-- Main hero unit for a primary marketing message or call to action -->
  <div class="hero-unit">
    <h1><?php echo ($this->pageTitle) ? $this->pageTitle : 'piano.base'; ?></h1>
    <p>SpaghettiPHP framework + Bootstrap, from Twitter (v2.0.2)</p>
    <p><a href="https://github.com/pianolab/spaghetti-base" class="btn btn-inverse btn-large">piano.base source code</a></p>
  </div>
</header>