<?php

require_once APP . DS . 'vendor' . DS . 'mailer' . DS . 'Mailer.php';

class ContactController extends AppController {
  
  /**
   * Método de envio de e-mail padrão
   *
   * @return void
   * @author Djalma Araújo
   */
  public function index() {

    // checa se a POST
    if(!empty($this->data)) {
      // validação
      if ($this->Contact->validate($this->data)) {
        // configuração de email
        $mailer = new Mailer(array(
          'from' => array($this->data['email'] => $this->data['name']),
          'to' => MAILER_SEND_DEFAULT,
          'subject' => 'Formulário de Contato [ ' . APP_NAME . ' ]',
          'views' => array(
            'text/plain' => 'contact/mail_contact.txt',
            'text/html' => 'contact/mail_contact.htm'
          ),
          'layout' => 'mail',
          'data' => array('data' => $this->data)
        ));

        // salvando o contato
        if ($this->Contact->save($this->data)) {
          // enviando email
          $mailer->send();
          Session::writeFlash('site.alert', array('success', 'Sua mensagem foi enviada com sucesso. <br /> Entreremos em contato com você o mais breve possível. Obrigado.'));
          $this->redirect('/');
        } 
        // não conseguiu salvar
        else {
          Session::writeFlash('site.alert', array('warning', 'Preencha os campos corretamente.'));
          Session::writeFlash('form.data', $this->data);
          $this->redirect('/contato');
        }
      }
      // dados enviados não são validos
      else {
        Session::writeFlash('site.alert', array('error', $this->Contact->errors));
        Session::writeFlash('form.data', $this->data);
        $this->redirect('/contato');
      }
    }

    // pega todos os contatos salvos no banco de dados
    $this->arrView['contacts'] = $this->Contact->all();
  }
}